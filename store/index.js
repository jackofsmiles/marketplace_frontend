import axios from 'axios'

// TODO split on vuex modules
export const state = () => ({
  sidebar: false,
  products: []
})

export const mutations = {
  toggleSidebar (state) {
    state.sidebar = !state.sidebar
  },
  addProduct (state, product) {
    state.sidebar = !state.sidebar
  }
}

export const actions = {
  addProduct (context, product) {
    context.commit('addProduct', product)
  }
}
